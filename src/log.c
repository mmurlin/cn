#include <stdio.h>
#include <stdarg.h>

#include "opts.h"
#include "log.h"

void _log_verbose(char const *format, ...)
{
    if (option_variables.verbose)
    {
        va_list args;

        va_start(args, format);
        vfprintf(stdout, format, args);
        va_end(args);
    }
}
