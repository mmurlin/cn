#include <stdio.h>
#include <stdlib.h>

#include "opts.h"
#include "log.h"

// Placeholder definitions, only defined after yaccing.
int parse();

int main(int argc, char **argv)
{
    int optind = getOpts(argc, argv);

    if (optind >= argc)
    {
        fprintf(stderr, "No files provided.\n");
        exit(1);
    }

    while (optind < argc)
    {
        _log_verbose("\nCompiling file %s...\n", argv[optind]);
        if (parse(argv[optind]))
        {
            // TODO Write a function to log errors more nicely, to be consistent with getopt.
            fprintf(stderr, "Error writing to file %s\n", argv[optind]);
            exit(1);
        }
        _log_verbose("Successfully compiled %s\n", argv[optind]);
        ++optind;
    }

    // TODO We have a lot of other exit paths, need to exit gracefully and free there too.
    free(option_variables.output_dir);
    exit(0);
}
