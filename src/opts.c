#include "opts.h"

struct optvars option_variables = {
    .verbose = false,
    .preserve_dir = false,
    .output_dir = NULL
};

static struct option const long_options[] = {
    {"help",          no_argument,        0, 'h' },
    {"version",       no_argument,        0, 'V' },
    {"verbose",       no_argument,        0, 'v' },
    {"output-dir",    required_argument,  0, 'o' },
    // Opts with no short version:
    {"preserve-dir",  no_argument,        &option_variables.preserve_dir, true },
    {0,             0,                    0,  0  }
};

int getOpts(int argc, char **argv)
{


    while (1)
    {
        int option_index = 0;

        int c = getopt_long(argc, argv, "o:vVh",
                            long_options, &option_index);

        if (c == -1)
            // Exhausted argv, leave loop:
            break;

        switch (c) {
            case 'V':
                puts(VERSION_STRING);
                exit(0);
            case 'v':
                option_variables.verbose = true;
                break;
            case 'o':
                option_variables.output_dir = (char *)malloc(FILENAME_MAX+1);
                strncpy(option_variables.output_dir, optarg, FILENAME_MAX+1);
                break;
            case 0:
                // This case deals with all valid longopts without a short version.
                // If just a flag, write to variable (e.g. --preserve-dir).
                // If needed, struct entry is long_option[option_index].
                break;
            case 'h':
                printf(HELP_STRING);
                exit(0);
            default:
                printf(HELP_STRING);
                exit(2);
        }
    }

    _log_verbose("Compiling files with options:\n"
                 "    verbose = %i\n"
                 "    preserve_dir = %i\n"
                 "    output_dir = %s\n",
                 option_variables.verbose,
                 option_variables.preserve_dir,
                 option_variables.output_dir);

    return optind;
}
