%{

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libgen.h>
#include <stdbool.h>

// For filechecking. Linux dependent.
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "log.h"
#include "opts.h"
#include "curly.h"

// Flex Definitions
extern int yylex();
extern int yyparse();
extern FILE *yyin;

// Declared in cn.l
extern int yylineno;

void yyerror(const char *s);

// Required Globals
FILE *fcpp;
FILE *fhpp;

int indentBlockDepth = 0;
int indentPrev = 0;
int indentFloor = 0;

// God this is awful.
char functionPrefixes[8][100];

char prevExpr[100];

// Function Definitions
void addBracketsAndSemicolons(FILE *fout, int indentCurr, int indentBlockDepth, int *indentPrev, bool cpp);
void handleClasses(char const *expr);
void getOutputDir(char *inputFilepath, char **outputDir);
void getOutputFile(char *inputFilepath, char **outputFilepath);
int parse(char *finpath);

%}

%union {
    int ival;
    char *sval;
}

%token NL
%token START_INDENT
%token INDENT

%token <sval> MACRO
%token <sval> EXPR

%type <ival> line_indent
%type <sval> codeline

%start codefile

%%

codefile:
        | codefile line         { }
;

line:
        NL                      {
                                    fprintf(fhpp, "\n");
                                    fprintf(fcpp, "\n");
                                }
        | codeline NL           {
                                    free($1);
                                    fprintf(fhpp, "\n");
                                    fprintf(fcpp, "\n");
                                }
;

codeline:
        MACRO                   {
                                    fprintf(fhpp, "%s", $1);
                                    $$ = $1;
                                }
        | EXPR                  {
                                    if (indentPrev > 0)
                                    {
                                        addBracketsAndSemicolons(fcpp, indentFloor, indentBlockDepth, &indentPrev, true);
                                    }

                                    indentFloor = 0;

                                    addBracketsAndSemicolons(fhpp, 0, indentBlockDepth, &indentPrev, false);
                                    fprintf(fhpp, "%s", $1);

                                    // NOTE: strcpy is not necessarily safe.
                                    //       This should be used cautiously.
                                    strcpy(prevExpr, $1);

                                    handleClasses($1);

                                    $$ = $1;
                                }
        | line_indent EXPR      {
                                    if (indentBlockDepth == 0)
                                    {
                                        indentBlockDepth = $1;
                                        indentFloor *= indentBlockDepth;
                                    }

                                    if ($1 > indentFloor)
                                    {
                                        if (indentPrev == indentFloor)
                                        {
                                            // If move above floor, write previous and new expr to cpp.

                                            char temp[100];
                                            char *ident = strstr(prevExpr, "(");
                                            do { --ident; } while (ident != prevExpr && *(ident-1) != ' ');
                                            int const diff = ident - prevExpr;
                                            strncpy(temp, prevExpr, diff);
                                            temp[diff] = '\0';
                                            strcat(temp, functionPrefixes[indentFloor / indentBlockDepth]);
                                            strcat(temp, ident);

                                            fprintf(fcpp, "%s\n", temp);
                                        }
                                        addBracketsAndSemicolons(fcpp, $1, indentBlockDepth, &indentPrev, true);
                                        fprintf(fcpp, "%s", $2);
                                    }
                                    else
                                    {
                                        if (indentPrev > indentFloor)
                                        {
                                            // If move below floor, close remaining brackets in implementation.
                                            addBracketsAndSemicolons(fcpp, indentFloor, indentBlockDepth, &indentPrev, true);
                                        }

                                        indentFloor = $1;

                                        addBracketsAndSemicolons(fhpp, $1, indentBlockDepth, &indentPrev, false);
                                        fprintf(fhpp, "%s", $2);
                                    }

                                    // NOTE: strcpy is not necessarily safe.
                                    //       This should be used cautiously.
                                    //       Currently vulnerable to buffer overflow
                                    strcpy(prevExpr, $2);

                                    handleClasses($2);

                                    $$ = $2;
                                }

line_indent:
        START_INDENT            { $$ = 1; }
        | line_indent INDENT    { $$ = $1 + 1; }
;

%%

void handleClasses(char const *expr)
{
    if (strncmp(expr, "class ", 6) == 0)
    {
        indentFloor += (indentBlockDepth) ? indentBlockDepth : 1;

        char const *temp = expr + 6; // Move ahead by strlen("class ")
        char const *p = temp;
        // Iterate on string until word ends
        do { ++p; } while (*p != '\0' && *p != ' ');
        // Place class name into current floor of functionPrefixes (ie scope)
        int x = indentFloor;
        if (indentBlockDepth) { x /= indentBlockDepth; }
        strncpy(functionPrefixes[x], temp, p - temp);
        functionPrefixes[x][p - temp + 1] = '\0';
        strcat(functionPrefixes[x], "::");
    }
}


void getOutputDir(char *inputFilepath, char **outputDir)
{
    _log_verbose("Determining output directory...\n");
    if (option_variables.preserve_dir && option_variables.output_dir)
    {
        fprintf(stderr, "Options --preserve-dir and -o/--output-dir cannot be used together.\n"
                        "Use --help for more information.\n");
        free(*outputDir);
        free(option_variables.output_dir);
        exit(2);
    }
    else if (option_variables.preserve_dir)
    {
        char *temp = (char *)malloc(FILENAME_MAX+1);
        strncpy(temp, inputFilepath, FILENAME_MAX+1);
        strncpy(*outputDir, dirname(temp), FILENAME_MAX+1);
        free(temp);
        _log_verbose("Preserving directory (%s)\n", *outputDir);
    }
    else if (option_variables.output_dir)
    {
        char *pch = strchr(option_variables.output_dir, '\0') - 1;
        if (*pch == '/' || *pch == '\\')
        {
            *pch = '\0';
        }

        strncpy(*outputDir, option_variables.output_dir, FILENAME_MAX+1);
        _log_verbose("Using given directory (%s)\n", *outputDir);
    }
    else
    {
        strncpy(*outputDir, ".\0", 2);
        _log_verbose("Using current directory (.)\n", *outputDir);
    }

    // Check if directory exists.
    struct stat s;
    if (stat(*outputDir, &s) != 0 || !S_ISDIR(s.st_mode))
    {
        fprintf(stderr, "%s does not exist or is not a directory.\n", *outputDir);
        free(*outputDir);
        exit(2);
    }
}

// Essentially glibc implementation for basename.
char *getBasename(const char *filepath)
{
    char *pch;
#if defined(_WIN32) || defined(WIN32)
    pch = strrchr(filepath, '\\');
#else
    pch = strrchr(filepath, '/');
#endif
    return pch ? pch + 1 : (char *) filepath;
}

void getOutputFile(char *inputFilepath, char **outputFilepath)
{
    getOutputDir(inputFilepath, outputFilepath);

    char *pch = strchr(*outputFilepath, '\0');
#if defined(_WIN32) || defined(WIN32)
    strncpy(pch, "\\\0", 2);
#else
    strncpy(pch, "/\0", 2);
#endif
    size_t inLen = strlen(inputFilepath)+1;
    char *temp = (char *)malloc(inLen);
    strncpy(temp, inputFilepath, inLen);
    char *temp_pch = getBasename(temp);
    // I think pch - outputFilepath + FILENAME_MAX can be gt FILENAME_MAX at edgecase.
    strncpy(pch+1, temp_pch, strlen(temp_pch)+1);
    free(temp);
}

int parse(char *filepath)
{
    _log_verbose("Opening file for parsing...\n", filepath);
    FILE *fin = fopen(filepath, "r");
    if (!fin)
    {
        return 1;
    }
    yyin = fin;

    char *outputFilepath = (char *)malloc(FILENAME_MAX+1);
    char *pch;
    getOutputFile(filepath, &outputFilepath);

    char cppFilepath[strlen(outputFilepath)+2];
    pch = strstr(outputFilepath, ".cn");
    strncpy(pch, ".cpp\0", 5);
    strncpy(cppFilepath, outputFilepath, strlen(outputFilepath)+2);

    char hppFilepath[strlen(outputFilepath)+2];
    pch = strstr(outputFilepath, ".cpp");
    strncpy(pch, ".hpp\0", 5);
    strncpy(hppFilepath, outputFilepath, strlen(outputFilepath)+2);

    fcpp = fopen(cppFilepath, "w+");
    fhpp = fopen(hppFilepath, "w+");

    fprintf(fcpp, "#include \"%s\"", basename(outputFilepath));

    free(outputFilepath);

    _log_verbose("Parsing %s...\n", filepath);
    do
    {
        yyparse();
    } while(!feof(yyin));
    _log_verbose("Finished parsing %s\n", filepath);

    // Close any open brackets.
    addBracketsAndSemicolons(fcpp, indentFloor, indentBlockDepth, &indentPrev, true);
    addBracketsAndSemicolons(fhpp, 0, indentBlockDepth, &indentPrev, false);

    _log_verbose("Closing files...\n");
    fclose(fin);
    fclose(fhpp);
    fclose(fcpp);
}

void addBracketsAndSemicolons(FILE *fcpp, int indentCurr, int indentBlockDepth, int *indentPrev, bool cpp)
{
    int err = _addBracketsAndSemicolons(fcpp, indentCurr, indentBlockDepth, indentPrev, cpp);

    if (err == 1)
    {
        yyerror("Inconsistent indentation block width");
    }
}

void yyerror(const char *s) {
    fprintf(stderr, "(Line: %d) Parsing error: %s\n", yylineno, s);
    exit(1);
}
