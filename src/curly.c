#include "curly.h"

int _addBracketsAndSemicolons(FILE *fout, int indentCurr, int indentBlockDepth, int *indentPrev, bool cpp)
{
    int tempPrev = (indentBlockDepth) ? *indentPrev / indentBlockDepth : 0;
    int tempCurr = (indentBlockDepth) ? indentCurr / indentBlockDepth : 0;

    if (indentCurr != 0)
    {
        if (indentCurr % indentBlockDepth != 0)
        {
            return 1;
        }
    }

    int indentDiff = tempCurr - tempPrev;

    if (indentDiff > 0)
    {
        for (int i = 0; i < indentDiff; ++i)
        {
            fprintf(fout, "{");
        }
    }
    else
    {
        fprintf(fout, ";");
    }

    if (indentDiff < 0)
    {
        for (int i = 0; i < -indentDiff; ++i)
        {
            if (cpp)
            {
                fprintf(fout, "}");
            }
            else
            {
                fprintf(fout, "};");
            }
        }
    }

    *indentPrev = indentCurr;

    return 0;
}
