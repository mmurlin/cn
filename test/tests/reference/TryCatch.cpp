#include "TryCatch.hpp"

int main()
{
	try
	{
		return 0;
	}
	catch (...)
	{
		std::cerr << "ERROR";
	}
}
