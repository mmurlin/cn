#include "FunctionOverloading.hpp"

int f()
{
	return 0;
}

int f(int &a)
{
	a++;
	return 0;
}

int main()
{
	return 0;;
}