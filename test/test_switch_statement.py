from utils import compile_cn, compare_cpp_files, compare_hpp_files, run_cpp
import pytest

@pytest.mark.parametrize('compile_cn', ['SwitchStatement'], indirect=['compile_cn'])
def test_cpp(compile_cn):
    compare_cpp_files('SwitchStatement.cpp')


@pytest.mark.parametrize('compile_cn', ['SwitchStatement'], indirect=['compile_cn'])
def test_hpp(compile_cn):
    compare_hpp_files('SwitchStatement.hpp')


@pytest.mark.parametrize('compile_cn', ['SwitchStatement'], indirect=['compile_cn'])
def test_code(compile_cn):
    run_cpp('SwitchStatement.cpp')