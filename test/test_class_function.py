from utils import compile_cn, compare_cpp_files, compare_hpp_files, run_cpp
import pytest

@pytest.mark.parametrize('compile_cn', ['ClassFunction'], indirect=['compile_cn'])
def test_cpp(compile_cn):
    compare_cpp_files('ClassFunction.cpp')

@pytest.mark.parametrize('compile_cn', ['ClassFunction'], indirect=['compile_cn'])
def test_hpp(compile_cn):
    compare_hpp_files('ClassFunction.hpp')

@pytest.mark.parametrize('compile_cn', ['ClassFunction'], indirect=['compile_cn'])
def test_code(compile_cn):
    run_cpp('ClassFunction.cpp')