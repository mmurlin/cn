#ifndef OPTS_H
#define OPTS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <getopt.h>

#include "log.h"

// TODO A version string would look much nicer someplace else, eg a config file.
static char const * const VERSION_STRING = "cnatural version 0.0.0";

// TODO It would be preferable if the help string was tied to
//  eg long_option, a la docopt. Should we use docopt?
static char const * const HELP_STRING =
    "cn [options] files...\n"
    "\n"
    "Options:\n"
    "-h --help\n"
    "   Show this help message.\n"
    "-V --version\n"
    "   Show software version.\n"
    "-v --verbose\n"
    "   Provide detailed logging information.\n"
    "-o --output-dir <dir>\n"
    "   Output target source files in <dir> instead of present working directory.\n"
    "--preserve-dir\n"
    "   Output target source files to directory of input source file.\n";

struct optvars {
    bool verbose;
    int preserve_dir;
    char *output_dir;
};

extern struct optvars option_variables; // Defined in opts.c

int getOpts(int argc, char **argv);

#endif
