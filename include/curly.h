#ifndef CURLY_H
#define CURLY_H
#include <stdio.h>
#include <stdbool.h>

int _addBracketsAndSemicolons(FILE *fout, int indentCurr, int indentBlockDepth, int *indentPrev, bool cpp);
#endif
